import clsx from 'clsx'
import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import { KTSVG, toAbsoluteUrl } from '../../../helpers'
import { useLayout } from '../../core'
import { Topbar } from './Topbar'
import Select from 'react-select'

const options = [
    { value: 'demo', label: 'Demo' },
    { value: 'demo1', label: 'Demo-1' },
    { value: 'demo2', label: 'Demo-2' },
];

export default function HeaderMenu() {
    const { config, classes, attributes } = useLayout()
    const { header, aside } = config
    const [selectedOption, setSelectedOption] = useState(
        { value: 'demo', label: 'Demo' }
    )

    return (
        <div
            id='kt_header'
            className={clsx('header', classes.header.join(' '), 'align-items-stretch')}
            {...attributes.headerMenu}
        >
            <div
                className={clsx(
                    classes.headerContainer.join(' '),
                    'd-flex align-items-stretch justify-content-between'
                )}
            >
                <div className='d-flex align-items-center ms-n3 me-1'>
                    <div className="App header-select">
                        <Select
                            defaultValue={selectedOption}
                            // onChange={setSelectedOption}
                            options={options}
                        />
                    </div>
                </div>

                {/* begin::Aside mobile toggle */}
                {aside.display && (
                    <div className='d-flex align-items-center d-lg-none ms-n3 me-1' title='Show aside menu'>
                        <div
                            className='btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px'
                            id='kt_aside_mobile_toggle'
                        >
                            <KTSVG path='/media/icons/duotune/abstract/abs015.svg' className='svg-icon-2x mt-1' />
                        </div>
                    </div>
                )}
                {/* end::Aside mobile toggle */}

                {/* begin::Logo */}
                {!aside.display && (
                    <div className='d-flex align-items-center flex-grow-1 flex-lg-grow-0'>
                        <Link to='/perform-summary' className='d-lg-none'>
                            <img alt='Logo' src={toAbsoluteUrl('/media/logos/mScanIt-1.png')} className='h-30px' />
                        </Link>
                    </div>
                )}
                {/* end::Logo */}

                {aside.display && (
                    <div className='d-flex align-items-center flex-grow-1 flex-lg-grow-0'>
                        <Link to='/' className='d-lg-none'>
                            <img alt='Logo' src={toAbsoluteUrl('/media/logos/mScanIt-1.png')} className='h-30px' />
                        </Link>
                    </div>
                )}

                {/* begin::Wrapper */}
                <div className='d-flex align-items-stretch justify-content-between flex-lg-grow-1'>
                    {/* begin::Navbar */}
                    {header.left === 'menu' && (
                        <div className='d-flex align-items-stretch' id='kt_header_nav'>
                            {/*<Header />*/}
                        </div>
                    )}

                    {header.left === 'page-title' && (
                        <div className='d-flex align-items-center' id='kt_header_nav' />
                    )}



                    <div className='d-flex align-items-stretch flex-shrink-0'>
                        <div className='d-flex align-items-center ms-n3 me-1'>
                            Expand
                        </div>
                        <Topbar />
                    </div>

                </div>
            </div>
        </div>
    )
}
