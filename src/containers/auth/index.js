/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react'
import { useLocation, Route } from 'react-router-dom'
import { Registration } from '../../components/auth/Registration'
import ForgotPassword from '../../components/auth/ForgotPassword'
import Login from '../../components/auth/Login'
import { toAbsoluteUrl } from '../../theme/helpers'
import routeConfig from '../../routing/routeConfig'
import { colors } from '../../utils/constants'
import ResetPassword from '../../components/auth/ResetPassword'
import MerchantLogin from '../../components/merchantLogin/Login'
import { Link, Switch, Redirect } from "react-router-dom";

export function AuthPage(props) {
  const query = useLocation().search
  const pathName = useLocation().pathname

  useEffect(() => {
    document.body.classList.add('bg-white')
    return () => {
      document.body.classList.remove('bg-white')
    }
  }, [])

  const GetComponent = () => {
    const pathName = useLocation().pathname
    if (pathName === routeConfig.registration) {
      return <Registration />
    }
    if (pathName === routeConfig.forgotPassword) {
      return <ForgotPassword query={query} />
    }
    if (pathName === routeConfig.resetPassword) {
      return <ResetPassword />
    }
    if (pathName === routeConfig.merchantLogin) {
      return <MerchantLogin />
    }
    return <Login />
  }

  return (
    <>
      {
        pathName === routeConfig.userSdk || pathName === routeConfig.import ? (
          <>
            <Route path='/merchant-login' component={MerchantLogin} />
          </>
        ) :
          (

            <div className="d-flex flex-column flex-root">
              <div
                className="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white"
                id="kt_login"
              >
                <div
                  className="login-aside login-bg d-flex flex-row-auto bgi-size-cover bgi-no-repeat p-10 p-lg-10"
                  style={{
                    backgroundImage: `url("/media/bg-4.jpg")`,
                  }}
                >
                  <div className="d-flex flex-row-fluid flex-column justify-content-between">
                    <Link to="/" className="flex-column-auto mt-5 pb-lg-0 pb-10">

                    </Link>
                    <div className="flex-column-fluid d-flex flex-column justify-content-center">
                      <h2 className="font-size-h1 mb-5 text-white">
                        Welcome to mScanIt!
                      </h2>
                      <h4 className="font-size-h1 mb-5 text-white">
                        eCommerce Analytics for:
                      </h4>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Availability of stocks
                      </p>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Pricing and discounting
                      </p>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Media Share / Share of Voice / Share of Shelf
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </p>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Reviews and Ratings
                      </p>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Seller Analytics
                      </p>
                      <p className="font-weight-lighter text-white opacity-80">
                        - Competition Tracking
                      </p>
                    </div>
                    <div className="d-lg-flex mt-10 text-right">
                      <div className="opacity-70 font-weight-bold	text-white w-100">
                        &copy; 2020 mFilterIt
                      </div>
                    </div>
                  </div>
                </div>
                {GetComponent()}
                {/* <div className="d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
            <div className="position-absolute top-0 right-0 text-right mt-5 mb-15 mb-lg-0 flex-column-auto justify-content-center py-5 px-10">
              <span className="font-weight-bold text-dark-50">
                Don't have an account yet?
              </span>
              <Link
                to="/auth/registration"
                className="font-weight-bold ml-2"
                id="kt_login_signup"
              >
                Sign Up!
              </Link>
            </div>
            <div className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
              {GetComponent()}
            </div>
            <div className="d-flex d-lg-none flex-column-auto flex-column flex-sm-row justify-content-between align-items-center mt-5 p-5">
              <div className="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">
                &copy; 2020 Metronic
              </div>
              <div className="d-flex order-1 order-sm-2 my-2">
                <Link to="/terms" className="text-dark-75 text-hover-primary">
                  Privacy
                </Link>
                <Link
                  to="/terms"
                  className="text-dark-75 text-hover-primary ml-4"
                >
                  Legal
                </Link>
                <Link
                  to="/terms"
                  className="text-dark-75 text-hover-primary ml-4"
                >
                  Contact
                </Link>
              </div>
            </div>
          </div> */}
              </div>
            </div>
          )
      }
    </>
  )
}
