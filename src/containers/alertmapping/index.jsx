import React from 'react'
import Breadcrumb from "../../shared-components/breadcrumb";
import { TextField } from "../../shared-components/forms/TextField";
import { Formik, Form, Field, FieldArray } from "formik";
import { Row, Col, Button, Card } from "react-bootstrap";
import SelectField from "../../shared-components/forms/Select";
import * as yup from "yup";
import CustomTable from '../../shared-components/table/CustomTable';
import { columns } from '../mappingtable/Column';
import { data } from '../mappingtable/dummyData';

export default function AlertMapping() {

  const mapping_filters = [
    { value: "1", label: "Brand1" },
    { value: "2", label: "Brand2" },
    { value: "3", label: "Brand3" },
    { value: "4", label: "Brand14" },
  ];
  const schema = yup.object().shape({

    alert_group_name: yup.string().required(),
    platform: yup.string().required(),
    category: yup.string().required(),
    sub_category: yup.string().required(),

    brand_name: yup.string().required(),
    sub_brand_name: yup.string().required(),
    sku_range: yup.string().required(),
  });

  return (
    <div>
      <Breadcrumb path="/alert-group" labelname="Alert Group" />
      <div className="card-box">
        <Formik
          validationSchema={schema}
          initialValues={{
            alert_group_name: "",
            platform: "",
            category: "",
            sub_category: '',
            brand_type: '',
            brand_name: '',
            sub_brand_name: '',
            sku_range: '',

          }}
          onSubmit={(values) => {
            alert(JSON.stringify(values, null, 2));
          }}
        >
          {({ errors, touched, values, setFieldValue, handleBlur }) => (
            <Form>
              <Row gutter={[20, 20, 20]}>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="alert_group_name"
                    component={TextField}
                    placeholder="Alert Group Name"
                    label="Alert Group Name"
                    setFieldValue={values}
                  />
                </Col>

                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="platform"
                    component={TextField}
                    placeholder="Platform"
                    label="Platform"
                    setFieldValue={values}
                  />
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="category"
                    component={TextField}
                    placeholder="Category"
                    label="Category"
                    setFieldValue={values}
                  />
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="sub_category"
                    component={TextField}
                    placeholder="Sub-Category"
                    label="Sub-Category"
                    setFieldValue={values}
                  />
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    component={SelectField}
                    name="input_type" label="Mapping Filters"
                    placeholder="Select Mapping Filters"
                    option={mapping_filters}
                    values={values}
                    setFieldValue={setFieldValue}
                    handleBlur={handleBlur}
                  />
                  {/* <Field
                  name="brand_type"
                  component={SelectField}
                  placeholder="Brand type"
                  label="Brand Type"
                  option={mapping_filters.mapping_filters}
                  setFieldValue={setFieldValue}
                /> */}
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="brand_name"
                    component={TextField}
                    placeholder="Brand_name"
                    label="Brand_name"
                    setFieldValue={values}
                  />
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="sub_brand_name"
                    component={TextField}
                    placeholder="Sub Brand Name"
                    label="Sub Brand Name"
                    setFieldValue={values}
                  />
                </Col>
                <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                  <Field
                    name="sku_range"
                    component={TextField}
                    placeholder="Sku Range"
                    label="Sku Range"
                    setFieldValue={values}
                  />
                </Col>
              </Row>
              <div className='d-flex justify-content-center'>
                <Button type="submit" >
                  Save
                </Button>
              </div>
            </Form>
          )}
        </Formik>
        <div className='table mt-10'>
          <Row>
            <Col xs={6} sm={6} md={6} lg={6} xl={6}>
              <Card>
                <Card.Body className="p-0">
                  <div className='client_url_table '>
                    <Card.Title> Client_url_table  </Card.Title>
                    <CustomTable columns={columns} data={data} />
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6} xl={6}>
              <Card>
                <Card.Body className="p-0">
                  <div className='competitions_url_table '>
                    <Card.Title> Competitions_url_table  </Card.Title>
                    <CustomTable columns={columns} data={data} />
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}
