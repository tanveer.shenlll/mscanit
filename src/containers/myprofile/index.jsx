/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Row, Col, Button, Card } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';

const initialValues = {
    name: 'mFilterIt',
    username: 'mFilterIt',
    email: 'mFilterIt@gmail.com',
    phone_number: '9856235698',
    id: null
}

const schema = yup.object().shape({
    name: yup.string().required(),
    phone_number: yup.string().required(),
});


export default function Myprofile() {
    return (
        <>
            <Breadcrumb path="/changepassword" labelname="My Profile" />
            <Card>
                <Card.Header>
                    <Card.Title>Edit Profile</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Row>
                        <Col xs={12} sm={12} md={3} lg={3} xl={3} align="center">
                            <img src="/media/avatars/150-2.jpg" alt="img" className="profile-img" />
                        </Col>
                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <Formik
                                enableReinitialize={true}
                                validationSchema={schema}
                                initialValues={initialValues}
                                onSubmit={(values, { resetForm }) => {
                                    resetForm();
                                    console.log(values);
                                }}
                            >
                                {({ values, setFieldValue, handleBlur }) => (
                                    <Form>
                                        <Row gutter={[20, 20]}>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="name"
                                                    component={TextField}
                                                    placeholder="Enter Name"
                                                    label="Name"
                                                    type="text"
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="username"
                                                    component={TextField}
                                                    placeholder="Enter Username"
                                                    label="User Name"
                                                    type="text"
                                                    readOnly={true}
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="email"
                                                    component={TextField}
                                                    placeholder="Enter Email ID"
                                                    label="Email"
                                                    type="email"
                                                    readOnly={true}
                                                />
                                            </Col>

                                            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                                <Field name="phone_number"
                                                    component={TextField}
                                                    placeholder="Enter phone number"
                                                    label="Phone Number"
                                                    type="number"
                                                />
                                            </Col>

                                        </Row>

                                        <div className="text-center">
                                            <Button type="submit" className="w-100 mt-5">
                                                Save
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </>
    )
}
