import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field, FieldArray } from "formik";
import SelectField from '../../shared-components/forms/Select';
import MultiSelectField from '../../shared-components/forms/MultiSelect';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';


const initialValues = {
    group_name: '',
    keyboard_url: '',
    country: '',
    ecom_platforms: '',
    input_type: '',
    friends: [''],
    id: null
}

const country = [
    { value: "1", label: "India" },
    { value: "2", label: "America" },
    { value: "3", label: "China" },
    { value: "4", label: "Russia" }
]

const ecom = [
    { value: "1", label: "Ecom1" },
    { value: "2", label: "Ecom2" },
    { value: "3", label: "Ecom3" },
    { value: "4", label: "Ecom4" }
]

const inputType = [
    { value: "keyword", label: "Keyword" },
    { value: "urls", label: "URLs" },
    { value: "productcode", label: "Product Code" },
]

const schema = yup.object().shape({
    group_name: yup.string().required(),
    keyboard_url: yup.string().required(),
    input_type: yup.string().required(),
});


export default function CreateOwnMarket() {


    const [marketList, setMarketList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = marketList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const marketListFilter =
            marketList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setMarketList(marketListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/create-own-market" labelname="CreateYourOwnMarket" />
            <Row>
                <Col xs={12} sm={12} md={4} lg={4} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {marketList && marketList.map((item) => (
                                <li className="list-group-item cursor-pointer" key={item.group_name} onClick={() => handleEditMarket(item)}>{item.group_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const marketLists = [...marketList, values]
                                setMarketList(marketLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);

                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field name="group_name"
                                                component={TextField}
                                                placeholder="group name"
                                                label="Group Name"
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field name="keyboard_url"
                                                component={TextField}
                                                placeholder="Keyword / URL Type"
                                                label="Keyword / URL Type" />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="country"
                                                label="Country"
                                                placeholder="Select Country"
                                                option={country}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="ecom_platforms"
                                                label="E-commerce platforms"
                                                placeholder="Select E-commerce platforms"
                                                option={ecom}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="input_type" label="Input Type"
                                                placeholder="Select Input Type"
                                                option={inputType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    </Row>
                                    {values.input_type === "keyword" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Keywords</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter Keywords" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    {values.input_type === "urls" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>URLs</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter URLs" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    {values.input_type === "productcode" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Product Code</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter Productcode" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    <div>
                                        <Button type="submit" className="m-5">{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
