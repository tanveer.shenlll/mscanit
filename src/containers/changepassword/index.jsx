/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Row, Col, Button, Card } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';

const initialValues = {
    current_password: '',
    new_password: '',
    confirm_password: '',
}

const schema = yup.object().shape({
    current_password: yup.string().required(),
    new_password: yup.string().required(),
    confirm_password: yup.string().required(),
});


export default function ChangePassword() {
    return (
        <>
            <Breadcrumb path="/changepassword" labelname="Change Password" />
            <Card className="reportfilter-Ctr">
                <Card.Header>
                    <Card.Title>Change Password</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Formik
                        enableReinitialize={true}
                        validationSchema={schema}
                        initialValues={initialValues}
                        onSubmit={(values, { resetForm }) => {
                            resetForm();
                            console.log(values);
                        }}
                    >
                        {({ values, setFieldValue, handleBlur }) => (
                            <Form>
                                <Row gutter={[20, 20]}>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Field
                                            name="current_password"
                                            component={TextField}
                                            placeholder="Current Password"
                                            label="Current Password"
                                            type="password"
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Field
                                            name="new_password"
                                            component={TextField}
                                            placeholder="New Password"
                                            label="New Password"
                                            type="password"
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Field
                                            name="confirm_password"
                                            component={TextField}
                                            placeholder="Confirm Password"
                                            label="Confirm Password"
                                            type="password"
                                        />
                                    </Col>

                                </Row>

                                <div className="text-center">
                                    <Button type="submit" className="w-100 mt-5">
                                        Save
                                    </Button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Card.Body>
            </Card>
        </>
    )
}
