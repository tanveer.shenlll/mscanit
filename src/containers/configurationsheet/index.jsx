import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import SelectField from '../../shared-components/forms/Select';
import MultiSelectField from '../../shared-components/forms/MultiSelect';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';

const initialValues = {
    configuration_name: '',
    group_name: '',
    location_id: '',
    resurence_id: '',
    flow_type: '',
    search_platform: '',
    option_type: '',
    id: null
}

const groupName = [
    { value: "group1", label: "Group1" },
    { value: "group2", label: "Group2" },
    { value: "group3", label: "Group3" },
]

const locaionID = [
    { value: "1", label: "192.168.05.23" },
    { value: "2", label: "192.148.05.23" },
    { value: "3", label: "192.168.05.53" },
]

const resurenceID = [
    { value: "1", label: "resurenceID1" },
    { value: "2", label: "resurenceID2" },
    { value: "3", label: "resurenceID3" },
]

const flowType = [
    { value: "1", label: "flowType1" },
    { value: "2", label: "flowType2" },
    { value: "3", label: "flowType3" },
]

const searchPlatform = [
    { value: "app", label: "App" },
    { value: "web", label: "Web" },
]

const optionType = [
    { value: "1", label: "option1" },
    { value: "2", label: "option2" },
    { value: "3", label: "option3" },
]

const schema = yup.object().shape({
    configuration_name: yup.string().required(),
});


export default function ConfigurationSheet() {


    const [marketList, setMarketList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = marketList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const marketListFilter =
            marketList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setMarketList(marketListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/configuration" labelname="Configuration Sheet" />
            <Row>
                <Col xs={12} sm={12} md={4} lg={4} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center mb-5">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {marketList && marketList.map((item) => (
                                <li className="list-group-item cursor-pointer" key={item.configuration_name}
                                    onClick={() => handleEditMarket(item)}>{item.configuration_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const marketLists = [...marketList, values]
                                setMarketList(marketLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);

                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field name="configuration_name"
                                                component={TextField}
                                                placeholder="Configuration Name"
                                                label="Configuration Name"
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="group_name"
                                                label="Group Name"
                                                placeholder="Select Group Name"
                                                option={groupName}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="location_id"
                                                label="LocationID"
                                                placeholder="Select LocationID"
                                                option={locaionID}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="resurence_id" label="Resurence ID"
                                                placeholder="Select Resurence ID"
                                                option={resurenceID}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="flow_type" label="Flow Type"
                                                placeholder="Select Flow Type"
                                                option={flowType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={MultiSelectField}
                                                name="search_platform"
                                                label="Search Platform"
                                                placeholder="Select Platform"
                                                option={searchPlatform}
                                                setFieldValue={setFieldValue}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="option_type" label="Option"
                                                placeholder="Select Option"
                                                option={optionType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>

                                    </Row>

                                    <div>
                                        <Button type="submit" className="m-5">{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
