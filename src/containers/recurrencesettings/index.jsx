import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import SelectField from '../../shared-components/forms/Select';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';
import TimePicker from '../../shared-components/forms/TimePicker';

const initialValues = {
    recurrence_name: '',
    frequency: '',
    day_preference: '',
    date_preference: '',
    timepreference: '',
    id: null
}

const frequencyType = [
    { value: "daily", label: "Daily" },
    { value: "weekly", label: "Weekly" },
    { value: "monthly", label: "Monthly" },
    { value: "fortnightly", label: "Fortnightly" },
];

const dayPreference = [
    { value: "sunday", label: "Sunday" },
    { value: "monday", label: "Monday" },
    { value: "tuesday", label: "Tuesday" },
    { value: "wednesday", label: "Wednesday" },
    { value: "thursday", label: "Thursday" },
    { value: "friday", label: "Friday" },
    { value: "saturday", label: "Saturday" },
];

const datePreference = [
    { value: "1", label: "1" },
    { value: "2", label: "2" },
    { value: "3", label: "3" },
    { value: "4", label: "4" },
    { value: "5", label: "5" },
    { value: "6", label: "6" },
    { value: "7", label: "7" },
    { value: "8", label: "8" },
    { value: "9", label: "9" },
    { value: "10", label: "10" },
    { value: "11", label: "11" },
    { value: "12", label: "12" },
    { value: "13", label: "13" },
    { value: "14", label: "14" },
    { value: "15", label: "15" },
];

const schema = yup.object().shape({
    recurrence_name: yup.string().required(),
    frequency: yup.string().required(),
});


export default function RecurrenceSettings() {

    const [locationList, setlocationList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = locationList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const locationListFilter =
            locationList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setlocationList(locationListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            setConfirmModalShow(false);
            // console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/recurrence-settings" labelname="Recurrence Settings" />
            <Row>
                <Col xs={12} sm={12} md={4} lg={4} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center mb-5">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {locationList && locationList.map((item) => (
                                <li className="list-group-item cursor-pointer"
                                    key={item.recurrence_name} onClick={() => handleEditMarket(item)}>
                                    {item.recurrence_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const locationLists = [...locationList, values]
                                setlocationList(locationLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);
                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field name="recurrence_name"
                                                component={TextField}
                                                placeholder="Recurrence ID"
                                                label="Recurrence ID"
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="frequency"
                                                label="Frequency"
                                                placeholder="Select Frequency"
                                                option={frequencyType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <TimePicker
                                                label="Time Preference"
                                                name="timepreference" />
                                        </Col>

                                    </Row>

                                    {values.frequency === "fortnightly" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="day_preference"
                                                label="Day Preference"
                                                placeholder="Select Day Preference"
                                                option={dayPreference}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    }

                                    {values.frequency === "weekly" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="day_preference"
                                                label="Day Preference"
                                                placeholder="Select Day Preference"
                                                option={dayPreference}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    }

                                    {values.frequency === "monthly" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="date_preference"
                                                label="Date Preference"
                                                placeholder="Select Date Preference"
                                                option={datePreference}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    }
                                    <div>
                                        <Button type="submit" className="m-5">{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
