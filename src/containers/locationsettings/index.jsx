import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field, FieldArray } from "formik";
import SelectField from '../../shared-components/forms/Select';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';


const initialValues = {
    location_name: '',
    location_type: '',
    friends: [''],
    id: null
}

const inputType = [
    { value: "city", label: "City" },
    { value: "zipcode", label: "Zip Code" },
]

const schema = yup.object().shape({
    location_name: yup.string().required(),
    location_type: yup.string().required(),
});


export default function Locationsettings() {

    const [locationList, setlocationList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = locationList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const locationListFilter =
            locationList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setlocationList(locationListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            setConfirmModalShow(false);
            // console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/location-settings" labelname="Location Settings" />
            <Row>
                <Col xs={12} sm={12} md={4} lg={4} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {locationList && locationList.map((item) => (
                                <li className="list-group-item cursor-pointer"
                                    key={item.location_name} onClick={() => handleEditMarket(item)}>
                                    {item.location_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const locationLists = [...locationList, values]
                                setlocationList(locationLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);

                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field name="location_name"
                                                component={TextField}
                                                placeholder="Location ID Name"
                                                label="Location ID Name"
                                            />
                                        </Col>

                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="location_type" label="Location Type"
                                                placeholder="Select Location Type"
                                                option={inputType}
                                                values={values}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                    </Row>
                                    {values.location_type === "city" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>City</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`}
                                                                        placeholder="Enter City" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }

                                    {values.location_type === "zipcode" &&
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <FieldArray
                                                name="friends"
                                                render={arrayHelpers => (
                                                    <div className="fieldarray-ctr">
                                                        <label>Zipcode</label>
                                                        {values.friends && values.friends.length > 0 && (
                                                            values.friends.map((friend, index) => (
                                                                <div key={index}>
                                                                    <Field name={`friends.${index}`} placeholder="Enter Zipcode" />
                                                                    {index === 0 ?
                                                                        <span onClick={() => {
                                                                            if (friend !== '') {
                                                                                arrayHelpers.insert(index, '')
                                                                            }
                                                                            else {
                                                                                alert("Alert");
                                                                            }
                                                                        }}>
                                                                            <i class="bi bi-plus-circle-fill"></i>
                                                                        </span>
                                                                        :
                                                                        <i class="bi bi-dash-circle-fill"
                                                                            onClick={() => arrayHelpers.remove(index)}></i>
                                                                    }
                                                                </div>
                                                            ))
                                                        )}

                                                    </div>
                                                )}
                                            />
                                        </Col>
                                    }
                                    <div>
                                        <Button type="submit" className="m-5">{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
