import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import SelectField from '../../shared-components/forms/Select';
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import ConfirmModal from '../../shared-components/modal/confirmmodal';


const initialValues = {
    filter_name: '',
    category: '',
    range_type: '',
    range_unit: '',
    ecom_platforms: '',
    input_type: '',
    friends: [''],
    id: null
}

const category = [
    { value: "1", label: "RBL" },
    { value: "2", label: "ITC" },
    { value: "3", label: "Sunfeast" },
    { value: "4", label: "Britania" }
]

const range_type = [
    { value: "1", label: "1" },
    { value: "2", label: "2" },
    { value: "3", label: "3" },
    { value: "4", label: "4" }
]
const range_unit = [
    { value: "1", label: "1" },
    { value: "2", label: "2" },
    { value: "3", label: "3" },
    { value: "4", label: "4" }
]

const schema = yup.object().shape({
    filter_name: yup.string().required(),
    range_type: yup.string().required(),
    range_unit: yup.string().required(),
    category: yup.string().required(),
});


export default function RangeFilter() {


    const [marketList, setMarketList] = useState([]);
    const [formValues, setFormData] = useState(initialValues);
    const [searchInput, setSearchInput] = useState(null);
    const [confirmModalShow, setConfirmModalShow] = useState(false);


    const handleEditMarket = (marketData) => {
        marketData.id = marketList.length + 1;
        setFormData(marketData);
    }

    const handleFilter = (searchInput) => {
        const marketListFilter =
            marketList.filter(item => Object.values(item).join('').toLowerCase().includes(searchInput.toLowerCase()));
        setMarketList(marketListFilter);
    }

    useEffect(() => {
        setTimeout(() => {
            handleFilter(searchInput);
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchInput]);


    const handleCreatUpdate = (formData, resetForm) => {
        if (formValues.id) {
            update(formData);
        } else {
            delete formData.id
            create(formData, resetForm);
        }
    }

    const update = (formData) => { }
    const create = (formData, resetForm) => { }

    const handleOnSubmit = (confirm) => {
        if (confirm) {
            console.log(formValues.id);
        }
    }

    return (
        <>
            <Breadcrumb path="/range-filter" labelname="Range Filter" />
            <Row>
                <Col xs={12} sm={12} md={4} lg={4} xl={4} className="card-box">
                    <div>
                        <Row className="align-items-center">
                            <Col>
                                <h3>LISTS</h3>
                            </Col>
                            <Col align="right">
                                <Button type="primary">
                                    Add New &nbsp;
                                    <i className="bi bi-plus-lg"></i>
                                </Button>
                            </Col>
                        </Row>
                        <div className="mb-5">
                            <input className="form-control" onChange={(e) => setSearchInput(e.target.value)} type="text" placeholder="Search..." />
                        </div>
                        <ul class="list-group">
                            {marketList && marketList.map((item) => (
                                <li className="list-group-item cursor-pointer" key={item.filter_name} onClick={() => handleEditMarket(item)}>{item.filter_name}</li>
                            ))}
                        </ul>
                    </div>
                </Col>

                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                    <div className="card-box">
                        <Formik
                            enableReinitialize={true}
                            validationSchema={schema}
                            initialValues={formValues}
                            onSubmit={(values, { resetForm }) => {
                                const marketLists = [...marketList, values]
                                setMarketList(marketLists);
                                resetForm();
                                handleCreatUpdate(values, resetForm);

                            }}
                        >
                            {({ values, setFieldValue, handleBlur }) => (
                                <Form>
                                    <Row gutter={[20, 20]}>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field name="filter_name"
                                                component={TextField}
                                                placeholder="Range filter name"
                                                label="Range Filter Name"
                                                handleBlur={handleBlur}
                                                setFieldValue={setFieldValue}
                                                values={values}

                                            />
                                        </Col>


                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="category"
                                                label="category"
                                                values={values}
                                                placeholder="Select category"
                                                option={category}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <Field
                                                component={SelectField}
                                                name="range_type"
                                                label="Range_type"
                                                values={values}
                                                placeholder="Select Range_type"
                                                option={range_type}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>
                                        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                            <Field
                                                component={SelectField}
                                                name="range_unit"
                                                label="Range_unit"
                                                values={values}
                                                placeholder="Select Range_unit"
                                                option={range_unit}
                                                setFieldValue={setFieldValue}
                                                handleBlur={handleBlur}
                                            />
                                        </Col>

                                        {/* <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <div>
                                                <Row className="d-flex justify-content-center">
                                                    <Col className='mt-5 ml-5'>
                                                        <h3>Range</h3>
                                                    </Col>
                                                    <Col className='mt-5 mx-auto'>
                                                        <h3>Name</h3>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col> */}
                                        <Row className='d-flex justify-content-center'>
                                            <Col xs={3} sm={3} md={3} xl={3} lg={6}>
                                                Range
                                            </Col>
                                            <Col className='ml-5 d-flex justify-content-center' xs={3} sm={3} md={3} xl={3} lg={6}>
                                                Name
                                            </Col>
                                        </Row>
                                    </Row>
                                    <div>
                                        <Button type="submit" className="m-5">{formValues.id ? 'Update' : 'Submit'}</Button>
                                        {formValues.id && (
                                            <Button onClick={() => setConfirmModalShow(true)}> Delete </Button>
                                        )}
                                    </div>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </Col>
            </Row>

            {confirmModalShow &&
                <ConfirmModal
                    show={confirmModalShow}
                    handleClose={() => setConfirmModalShow(false)}
                    handleSubmit={handleOnSubmit}
                    message="Do you want to delete this?"
                />
            }
        </>
    )
}
