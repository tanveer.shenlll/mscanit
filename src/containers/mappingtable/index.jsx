import React from 'react'
import { columns } from "./Column";
import { data } from "./dummyData";
import CustomTable from "../../shared-components/table/CustomTable";
import Breadcrumb from "../../shared-components/breadcrumb";
import { TextField } from "../../shared-components/forms/TextField";
import { Formik, Form, Field } from "formik";
import { Row, Col, Button } from "react-bootstrap";
import SelectField from "../../shared-components/forms/Select";
import * as yup from "yup";
import BootStrapForm from "react-bootstrap/Form";

const mapping_filters = [
  { value: "1", label: "Brand1" },
  { value: "2", label: "Brand2" },
  { value: "3", label: "Brand3" },
  { value: "4", label: "Brand14" },
];

const schema = yup.object().shape({
  search_group: yup.string().required()
});

export default function MappingTable() {
  return (
    <div>
      <Breadcrumb path="/mapping-table" labelname="Mapping Table" />
      <div className="card-box">
        <Formik
          validationSchema={schema}
          initialValues={{
            mapping_filters: "",
            search_group: "",
          }}
          onSubmit={(values) => {
            alert(JSON.stringify(values, null, 2));
          }}
        >
          {({ errors, touched, values, setFieldValue, handleBlur }) => (
            <Form>
              <Row gutter={[20, 20, 20]}>
                <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                  <Field
                    component={SelectField}
                    name="input_type" label="Mapping Filters"
                    placeholder="Select Mapping Filters"
                    option={mapping_filters}
                    values={values}
                    setFieldValue={setFieldValue}
                    handleBlur={handleBlur}
                  />
                </Col>

                <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                  <Field
                    name="search_group"
                    component={TextField}
                    placeholder="Search Group"
                    label="Search Group"
                    setFieldValue={setFieldValue}
                  />
                </Col>
                <Col xs={4} sm={4} md={4} lg={4} xl={4}>
                  <div>
                    <label style={{ width: "100%" }}>&nbsp;</label>
                    <Button type="submit" style={{ width: "100%" }}>
                      Search
                    </Button>
                  </div>
                </Col>

                <Col xs={12} sm={12} md={6} lg={6} xl={6}>

                  <div>
                    <BootStrapForm.Group controlId="formFile" className="mb-3">

                      <BootStrapForm.Control type="file" />
                    </BootStrapForm.Group>
                  </div>
                </Col>
                <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                  <Button className="w-100">
                    Download Sample CSV
                  </Button>{" "}
                </Col>
                <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                  <Button className="w-100">Download Mapping Data</Button>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
        <CustomTable data={data} columns={columns} />
      </div>
    </div>
  )
}
