/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import SelectField from '../../shared-components/forms/Select';
import MultiSelectField from '../../shared-components/forms/MultiSelect';
import * as yup from "yup";
import { Row, Col, Button, Card } from "react-bootstrap";
import Breadcrumb from '../../shared-components/breadcrumb';
import DateFieldicker from '../../shared-components/forms/DatePicker';

const initialValues = {
    start_date: '',
    category: '',
    sub_category: '',
    brand: '',
    sub_brand: '',
    report: '',
    id: null
}

const category = [
    { value: "1", label: "Category1" },
    { value: "2", label: "Category2" },
    { value: "3", label: "Category3" },
]

const subCategory = [
    { value: "1", label: "subCategory1" },
    { value: "2", label: "subCategory2" },
    { value: "3", label: "subCategory3" },
]

const brand = [
    { value: "1", label: "Brand1" },
    { value: "2", label: "Brand2" },
    { value: "3", label: "Brand3" },
]

const subBrand = [
    { value: "1", label: "subBrand1" },
    { value: "2", label: "subBrand2" },
    { value: "3", label: "subBrand3" },
]

const report = [
    { value: "basicecomm", label: "Basicecomm" },
    { value: "pricing", label: "Pricing" },
    { value: "discounting", label: "Discounting" },
    { value: "availability", label: "Availability" },
    { value: "bestseller", label: "Bestseller" },
    { value: "rating_review", label: "Rating_review" },

]

const schema = yup.object().shape({

});


export default function ConfigurationSheet() {
    const [marketList, setMarketList] = useState([]);

    return (
        <>
            <Breadcrumb path="/reports" labelname="Reports" />
            <Card className="reportfilter-Ctr">
                <Card.Header>
                    <Card.Title>Report Filter Download</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Formik
                        enableReinitialize={true}
                        validationSchema={schema}
                        initialValues={initialValues}
                        onSubmit={(values, { resetForm }) => {
                            const marketLists = [...marketList, values]
                            setMarketList(marketLists);
                            resetForm();
                            console.log(values);
                        }}
                    >
                        {({ values, setFieldValue, handleBlur }) => (
                            <Form>
                                <Row gutter={[20, 20]}>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Field name="start_date"
                                            component={DateFieldicker}
                                            placeholder="Start Date - End Date"
                                            label="Start Date - End Date"
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Field
                                            component={MultiSelectField}
                                            name="category"
                                            label="Category"
                                            placeholder="Select Category"
                                            option={category}
                                            setFieldValue={setFieldValue}
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Field
                                            component={MultiSelectField}
                                            name="sub_category"
                                            label="Sub Category"
                                            placeholder="Select Sub Category"
                                            option={subCategory}
                                            setFieldValue={setFieldValue}
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Field
                                            component={MultiSelectField}
                                            name="brand"
                                            label="Brand"
                                            placeholder="Select Brand"
                                            option={brand}
                                            setFieldValue={setFieldValue}
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                                        <Field
                                            component={MultiSelectField}
                                            name="sub_brand"
                                            label="Sub Brand"
                                            placeholder="Select Sub Brand"
                                            option={subBrand}
                                            setFieldValue={setFieldValue}
                                        />
                                    </Col>

                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <Field
                                            component={SelectField}
                                            name="report"
                                            label="Report"
                                            placeholder="Select Report"
                                            option={report}
                                            values={values}
                                            setFieldValue={setFieldValue}
                                            handleBlur={handleBlur}
                                        />
                                    </Col>

                                </Row>

                                <div className="text-center">
                                    <Button type="submit" className="w-100 mt-5">
                                        Download Report
                                    </Button>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </Card.Body>
            </Card>
        </>
    )
}
