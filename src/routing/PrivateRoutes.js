import React, { Suspense, lazy } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { FallbackView } from '../theme/partials'
import routeConfig from './routeConfig'

export function PrivateRoutes() {

  // const Login = lazy(() => import('../../src/components/auth/Login'))

  // const Merchant = lazy(() => import('../containers/merchant/index'))
  const DashboardPage = lazy(() => import('../containers/dashboard'))
  // const RiskManagement = lazy(() => import('../containers/riskManagement/index'))
  const CreateOwnMarket = lazy(() => import('../containers/createownmarket'))
  const LocationSettings = lazy(() => import('../containers/locationsettings'))
  const RecurrenceSettings = lazy(() => import('../containers/recurrencesettings'))
  const ConfigurationSheet = lazy(() => import('../containers/configurationsheet'))
  const MappingTable = lazy(() => import('../containers/mappingtable'))
  const CreateAlert = lazy(() => import('../containers/createalert'))
  const RangeFilter = lazy(() => import('../containers/rangefilter'))
  const AlertMapping = lazy(() => import('../containers/alertmapping'))
  const Reports = lazy(() => import('../containers/reports'))
  const CreateUser = lazy(() => import('../containers/createuser'))
  const ChangePassword = lazy(() => import('../containers/changepassword'))
  const Myprofile = lazy(() => import('../containers/myprofile'))
  return (
    <Suspense fallback={<FallbackView />}>
      <Switch>
        {<Redirect exact from="/" to="/dashboard" />}
        {/* <Route path={routeConfig.login} component={Login} /> */}
        <Route path={routeConfig.dashboard} component={DashboardPage} />
        <Route path={routeConfig.createownmarket} component={CreateOwnMarket} />
        <Route path={routeConfig.locationsettings} component={LocationSettings} />
        <Route path={routeConfig.recurrenceseettings} component={RecurrenceSettings} />
        <Route path={routeConfig.configurationsheet} component={ConfigurationSheet} />
        <Route path={routeConfig.mappingtable} component={MappingTable} />
        <Route path={routeConfig.createalert} component={CreateAlert} />
        <Route path={routeConfig.rangefilter} component={RangeFilter} />
        <Route path={routeConfig.alertmapping} component={AlertMapping} />
        <Route path={routeConfig.reports} component={Reports} />
        <Route path={routeConfig.createuser} component={CreateUser} />
        <Route path={routeConfig.changepassword} component={ChangePassword} />
        <Route path={routeConfig.myprofile} component={Myprofile} />

        {/* <Route path={routeConfig.riskManagement} component={RiskManagement} />
        <Route path={routeConfig.merchant} component={Merchant} /> */}
      </Switch>
    </Suspense>
  )
}
