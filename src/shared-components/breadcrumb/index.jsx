import React from 'react'
import { Row, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function Breadcrumb(props) {
    const { path, labelname } = props;
    return (
        <>
            <Row>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <h2>{labelname}</h2>
                </Col>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                    <div className="breadcrumbCtr">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Home</li>
                            <Link class="breadcrumb-item active" to={path}>{labelname}</Link>
                        </ol>
                    </div>
                </Col>
            </Row>
        </>
    )
}
