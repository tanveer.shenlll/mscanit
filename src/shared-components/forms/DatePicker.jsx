import React, { useState } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const DateFieldicker = React.forwardRef((props) => {
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;
    const { label, name, placeholder } = props;
    // const handleOnchange = (e) => {
    //     setFieldValue(field.name, e.value);
    // }

    return (
        <>
            <label>{label}</label>
            <DatePicker
                selectsRange={true}
                startDate={startDate}
                endDate={endDate}
                onChange={(update) => {
                    setDateRange(update);
                }}
                isClearable={true}
                name={name}
                placeholderText={placeholder}
            />
            {/* <Select options={option} name={field.name} placeholder={placeholder}
                className={touched[field.name] && errors[field.name] ? 'is-invalid' : values[field.name] ? 'is-valid' : ""}
                onChange={(option) => handleOnchange(option)} value={field.value ? option.find(option => option.value === field.value) : ''} /> */}
            {/* <span className="danger">{touched[field.name] && errors[field.name]}</span> */}
        </>
    )
});

export default DateFieldicker;