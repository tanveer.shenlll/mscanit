import React, { useState } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const TimePicker = React.forwardRef((props) => {
    const [startDate, setStartDate] = useState(new Date());
    const { label, name } = props;
    // const handleOnchange = (e) => {
    //     setFieldValue(field.name, e.value);
    // }

    return (
        <>
            <label>{label}</label>
            <DatePicker
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                showTimeSelect
                showTimeSelectOnly
                timeIntervals={60}
                name={name}
                timeCaption="Time"
                dateFormat="h:mm aa"
            />
            {/* <Select options={option} name={field.name} placeholder={placeholder}
                className={touched[field.name] && errors[field.name] ? 'is-invalid' : values[field.name] ? 'is-valid' : ""}
                onChange={(option) => handleOnchange(option)} value={field.value ? option.find(option => option.value === field.value) : ''} /> */}
            {/* <span className="danger">{touched[field.name] && errors[field.name]}</span> */}
        </>
    )
});

export default TimePicker;