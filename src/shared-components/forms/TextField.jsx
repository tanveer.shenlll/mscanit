import React from "react";

const getFieldCSSClasses = (touched, errors) => {
    const classes = ["form-control"];
    if (touched && errors) {
        classes.push("is-invalid");
    }

    if (touched && !errors) {
        classes.push("is-valid");
    }

    return classes.join(" ");
};

export function TextField({
    field, // { name, value, onChange, onBlur }
    form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    label,
    withFeedbackLabel = true,
    customFeedbackLabel,
    type,
    ...props
}) {
    return (
        <>

            {label && <label>{label}</label>}
            <input
                type={type}
                className={getFieldCSSClasses(touched[field.name], errors[field.name])}
                {...field}
                {...props}
            />
            <span className="danger">{touched[field.name] && errors[field.name]}</span>
            {withFeedbackLabel && (
                <span
                    error={errors[field.name]}
                    touched={touched[field.name]}
                    label={label}
                    type={type}
                    customFeedbackLabel={customFeedbackLabel}
                />
            )}
        </>
    );
}
