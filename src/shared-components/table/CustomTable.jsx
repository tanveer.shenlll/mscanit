import React from "react";
// import { data } from "../../containers/mappingtable/dummyData";
// import { columns } from "../../containers/mappingtable/Column";
import BootstrapTable from "react-bootstrap-table-next";

const selectRow = {
    mode: "checkbox",
    clickToSelect: true,
    hideSelectAll: false,
};
const customTotal = (from, to, size) => (
    <span className="react-bootstrap-table-pagination-total ml-3">
        Showing {from} to {to} of {size} Results
    </span>
);

const CustomTable = ({ data, columns }) => {
    const isEmpty = data;
    const emptyDataMessage = () => { return 'No Data to Display'; }

    return (
        <div>

            <BootstrapTable
                bootstrap4
                keyField="id"
                data={data}
                noDataIndication={'no results found'}
                columns={columns}

                wrapperClasses="table-responsive"
                classes="table table-head-custom  overflow-hidden"
                bordered={false}
                selectRow={selectRow} />


        </div>
    );
};

export default CustomTable;