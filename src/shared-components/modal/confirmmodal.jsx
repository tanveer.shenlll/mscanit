import React from 'react';
import { Button, Modal } from "react-bootstrap";

export default function ConfirmModal(props) {

    const { show, handleClose, message, handleSubmit } = props;

    return (
        <Modal show={show} backdrop="static">
            <Modal.Header>
                <Modal.Title>Confirm</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {message}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => handleClose(false)}>
                    Dimiss
                </Button>
                <Button variant="primary" onClick={() => handleSubmit(true)}>Confirm</Button>
            </Modal.Footer>
        </Modal>
    )
}
