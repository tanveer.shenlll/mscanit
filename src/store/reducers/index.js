import { combineReducers } from 'redux'
import { preventionReducer, editAlertsReducer, preventionStoreKey, editAlertsStoreKey } from './preventionAlert'
import loginReducer, { loginStoreKey } from './login'
import {
  forgotPasswordReducer,
  forgotPasswordStoreKey,
  verifyUserReducer,
  verifyUserStoreKey,
  resetPasswordStoreKey,
  resetPasswordReducer
} from './forgotPassword'
import { riskManagementlistReducer, riskManagementlistStoreKey } from './riskManagementList'

export default function createReducer () {
  const rootReducer = combineReducers({
    [preventionStoreKey]: preventionReducer,
    [editAlertsStoreKey]: editAlertsReducer,
    [loginStoreKey]: loginReducer,
    [forgotPasswordStoreKey]: forgotPasswordReducer,
    [verifyUserStoreKey]: verifyUserReducer,
    [resetPasswordStoreKey]: resetPasswordReducer,
    [riskManagementlistStoreKey]: riskManagementlistReducer
  })

  return rootReducer
}
