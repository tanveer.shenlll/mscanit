export const riskManagementActionsTypes = {
  GET_RISK_MANAGEMENT_LIST: 'GET_RISK_MANAGEMENT_LIST',
  SAVE_RISK_MANAGEMENT_LIST_RESPONSE: 'SAVE_RISK_MANAGEMENT_LIST_RESPONSE',
  CLEAR_RISK_MANAGEMENT_LIST: 'CLEAR_RISK_MANAGEMENT_LIST'
}

export const riskManagementActions = {
  getRiskManagementlist: (params) => ({
    type: riskManagementActionsTypes.GET_RISK_MANAGEMENT_LIST,
    params
  }),
  saveRiskManagementlistResponse: (data) => ({
    type: riskManagementActionsTypes.SAVE_RISK_MANAGEMENT_LIST_RESPONSE,
    data
  }),
  clearRiskManagementlist: () => ({
    type: riskManagementActionsTypes.CLEAR_RISK_MANAGEMENT_LIST
  })
}

