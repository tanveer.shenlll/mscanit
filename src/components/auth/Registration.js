/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { cancel } from 'redux-saga/effects';

const initialValues = {
  name: '',
  email: '',
  phone_no: '',
}

const schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  phone_no: yup.number().required(),
});

export function Registration(props) {

  return (
    <>
      <div className="login login-1 d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
        <div className="top-0 right-0 text-right mb-15 mb-lg-0 flex-column-auto py-5 px-10">
          <span className="font-weight-bold text-dark-50">
            Already have an account yet?
          </span>
          <Link
            to="/login"
            className="font-weight-bold ml-2"
            id="kt_login_signup"
          >
            Sign In
          </Link>
        </div>

        <div className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
          <div className="login-form login-signin">
            <div class="text-center mb-10 mb-lg-20 text-right">
              <h3 class="font-size-h1">Sign Up</h3>
              <p class="text-muted font-weight-bold mt-5">
                Make the most of your promotions
              </p>
              <p class="text-muted font-weight-bold">
                Get Started by Signing up!
              </p>
            </div>
            <Formik
              enableReinitialize={true}
              validationSchema={schema}
              initialValues={initialValues}
              onSubmit={(values, { resetForm }) => {
                window.location.href = '/login';
                resetForm();
              }}
            >
              {({ values, setFieldValue, handleBlur }) => (
                <Form>
                  <Row gutter={[20, 20]}>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="name"
                        component={TextField}
                        placeholder="Name"
                        label="Name"
                      />
                    </Col>

                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="email"
                        component={TextField}
                        placeholder="Email"
                        label="Email"
                        type="email"
                      />
                    </Col>

                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="phone_no"
                        component={TextField}
                        placeholder="Phone Number"
                        label="Phone Number"
                        type="number"
                      />
                    </Col>

                    <div class="form-group d-flex flex-wrap flex-center">
                      <button type="submit" id="kt_login_signup_submit"
                        class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</button>

                      <Link
                        to="/login"
                        className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4"
                        id="kt_login_signup">
                        Cancel
                      </Link>

                    </div>

                    {/* <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                      <a href="#" class="text-dark-50 text-hover-primary my-3 mr-2"
                        id="kt_login_forgot">Forgot Password ?</a>
                      <button type="submit" id="kt_login_signin_submit"
                        class="btn btn-primary font-weight-bold px-9 py-4 my-3">Sign In</button>
                    </div> */}

                    {/* <div>
                      <Button type="submit">Login</Button>
                    </div> */}
                  </Row>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </>
  )
}
