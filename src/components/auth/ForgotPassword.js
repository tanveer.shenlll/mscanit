/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { cancel } from 'redux-saga/effects';

const initialValues = {
  email: '',
}

const schema = yup.object().shape({
  email: yup.string().email().required(),
});

export default function ForgotPassword(props) {

  return (
    <>
      <div className="login login-1 d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
        <div className="top-0 right-0 text-right mb-15 mb-lg-0 flex-column-auto py-5 px-10">
          <span className="font-weight-bold text-dark-50">
            Already have an account yet?
          </span>
          <Link
            to="/login"
            className="font-weight-bold ml-2"
            id="kt_login_signup"
          >
            Sign In
          </Link>
        </div>

        <div className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
          <div className="login-form login-signin">
            <div class="text-center mb-10 mb-lg-20 text-right">
              <h3 class="font-size-h1">Forgotten Password ?</h3>
              <p class="text-muted font-weight-bold">
                We’ll send you an email with a reset link.
              </p>
            </div>
            <Formik
              enableReinitialize={true}
              validationSchema={schema}
              initialValues={initialValues}
              onSubmit={(values, { resetForm }) => {
                window.location.href = '/login';
                resetForm();
              }}
            >
              {({ values, setFieldValue, handleBlur }) => (
                <Form>
                  <Row gutter={[20, 20]}>

                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="email"
                        component={TextField}
                        placeholder="Email"
                        label="Email"
                        type="email"
                      />
                    </Col>

                    <div class="form-group d-flex flex-wrap flex-center">
                      <Button type="submit" id="kt_login_signup_submit"
                        class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">Submit</Button>

                      <Link
                        to="/login"
                        className="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4"
                        id="kt_login_signup">
                        Cancel
                      </Link>

                    </div>

                  </Row>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </>
  )
}
