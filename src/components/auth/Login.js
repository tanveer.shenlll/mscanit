/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { TextField } from '../../shared-components/forms/TextField';
import { Formik, Form, Field } from "formik";
import * as yup from "yup";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const initialValues = {
  user_name: '',
  password: ''
}

const schema = yup.object().shape({
  user_name: yup.string().required(),
  password: yup.string().required(),
});


export default function Login(props) {

  return (
    <>
      <div className="login login-1 d-flex flex-column flex-row-fluid position-relative p-7 overflow-hidden">
        <div className="top-0 right-0 text-right mb-15 mb-lg-0 flex-column-auto py-5 px-10 text-right">
          <span className="font-weight-bold text-dark-50">
            Don't have an account yet?
          </span>
          <Link
            to="/registration"
            className="font-weight-bold ml-2"
            id="kt_login_signup"
          >
            Sign Up!
          </Link>
        </div>

        <div className="d-flex flex-column-fluid flex-center mt-30 mt-lg-0">
          <div className="login-form login-signin">
            <div class="text-center mb-10 mb-lg-20">
              <h3 class="font-size-h1">Sign In</h3>
              <p class="text-muted font-weight-bold">Enter your username and password</p>
            </div>
            <Formik
              enableReinitialize={true}
              validationSchema={schema}
              initialValues={initialValues}
              onSubmit={(values, { resetForm }) => {
                localStorage.setItem('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvc3RhZ2luZ3VybC50a1wvdG1zLWFwaVwvcHVibGljXC9hcGlcL2VtcGxveWVlXC9sb2dpbiIsImlhdCI6MTY0ODk5NzQ5OCwiZXhwIjoxNjUwNTA5NDk4LCJuYmYiOjE2NDg5OTc0OTgsImp0aSI6ImNub3RDVGF1OFJzWWxDR2ciLCJzdWIiOjE0NCwicHJ2IjoiODlkMjU4NWU4ZTQ0NmIyMzNmMGM5YzFjNDIxMTU4MTY1ZDA2YjQ1MiJ9.F9i7LSbyy4upcr2sU_QXxFP4Q0S3dvYL--7FCPlosLE');
                window.location.href = '/dashboard';
                // resetForm();
              }}
            >
              {({ values, setFieldValue, handleBlur }) => (
                <Form>
                  <Row gutter={[20, 20]}>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="user_name"
                        component={TextField}
                        placeholder="User Name"
                        label="User Name"
                      />
                    </Col>

                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <Field name="password"
                        component={TextField}
                        placeholder="Password"
                        label="Password"
                        type="password"
                      />
                    </Col>


                    <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                      <Link to="/forgotPassword" class="text-dark-50 text-hover-primary my-3 mr-2"
                        id="kt_login_forgot">Forgot Password ?</Link>
                      <button type="submit" id="kt_login_signin_submit"
                        class="btn btn-primary font-weight-bold px-9 py-4 my-3">Sign In</button>
                    </div>

                    {/* <div>
                      <Button type="submit">Login</Button>
                    </div> */}
                  </Row>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </>
  )
}
