import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import color from "../../utils/colors";
import { useLocation, Link, useParams } from 'react-router-dom'
import { DATE, STATUS_RESPONSE } from "../../utils/constants";
import { DateSelector } from "../../theme/layout/components/DateSelector";
import ReactSelect from "../../theme/layout/components/ReactSelect";
import { toAbsoluteUrl } from '../../theme/helpers'
import {
  preventionAlertDetailsActions
} from "../../store/actions";

function Merchant(props) {
  const {
    getClientDispatch,
    getClient,
    getMerchantDispatch,
    getMerchant,
    addChargebackDispatch,
    clearAddChargebackDispatch,
    statusAC,
    messageAC,
    loadingAC,
    getPrevAlertDetailsDispatch,
    preventionAlerDetails
  } = props;

  console.log('xxxxxxxxxxxxxxxxxx',
    preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.address && preventionAlerDetails.risk_data.address[0].address
  )
  const IDPhone = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.phone
  const IDAddress = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.address
  const IDEmail = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.email
  const IDIP_Address = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.ip_address
  const IDNegative = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_overview && preventionAlerDetails.risk_overview.negative
  const IDPositive = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_overview && preventionAlerDetails.risk_overview.positive
  const IDPhoneNumber = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_data && preventionAlerDetails.risk_data.phone && preventionAlerDetails.risk_data.phone[0].number
  const Risk = preventionAlerDetails && preventionAlerDetails && preventionAlerDetails.risk_overview && preventionAlerDetails.risk_overview.risk_score

  console.log('IDAddress', IDAddress)
  const [alert, setAlert] = useState({
    type: "success",
    message: "",
  });
  let { idsss } = useParams();
  const url = useLocation().pathname
  const fields = url && url.split('/')
  const id = fields && fields[2]

  const [selectedClientOption, setSelectedClientOption] = useState("");
  const [clientOption, setClientOption] = useState();
  const [selectedMerchantOption, setSelectedMerchantOption] = useState("");
  const [merchantOption, setMerchantOption] = useState();
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if (id) {
      getPrevAlertDetailsDispatch(id)
    }
  }, [id])

  return (
    <>

      <div class="card mb-5 mb-xl-10">
        <div class="card-body pt-9 pb-0">
          <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
            <div class="flex-grow-1">
              <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                <div class="d-flex flex-column">
                  <div class="d-flex align-items-center mb-2">
                    <h1 class="d-flex align-items-center mb-5 fw-boldest my-1 fs-2">Risk Dashboard - Case ID #IRM100100 </h1>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 col-md-4 col-sm-4">
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-6" />
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <h2 className="mb-2 d-flex justify-content-start text-limegreen mb-5">Positive Responses</h2>
                      {
                        IDPositive && IDPositive.map((item, i) => {
                          return (
                            <span className="d-flex justify-content-start">
                              <i class="bi bi-check-circle-fill text-limegreen min-w-30px fsu " />
                              <h5 class="fw-bold fs-4 ">{item}</h5>
                            </span>
                          )
                        }
                        )
                      }
                    </div>
                  </div>

                </div>
                <div className="col-lg-4 col-md-4 col-sm-4">

                  <div className="d-flex justify-content-center">
                    <img
                      alt=''
                      src={toAbsoluteUrl('/media/credit/good-credit-score.jpg')}
                      className='h-200px me-3'
                    />

                  </div>
                </div>
                <div className="col-lg-4 col-md-4 col-sm-4">
                  <h2 className="mb-2 d-flex justify-content-start symbol-label text-darkorange mb-5">Negative Responses</h2>
                  {
                    IDNegative && IDNegative.map((item, i) => {
                      return (
                        <span className="d-flex justify-content-start">
                          <i class="bi bi-exclamation-triangle-fill text-darkorange min-w-30px fsu" />
                          <h5 class="text-darkorange fw-bold fs-4 ">{item}</h5>
                        </span>
                      )
                    }
                    )
                  }
                </div>

                <div className="row mb-0">
                  <div className="col-lg-4 col-md-4 col-sm-4" />
                  <div className="col-lg-4 col-md-4 col-sm-4">
                    <h1 class="d-flex justify-content-center  fw-boldest my-1 fs-2">Risk Score </h1>
                    <h1 class="d-flex justify-content-center  fw-boldest my-1 fs-2">{Risk} </h1>
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-4" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-3 col-md-3 col-sm-3">
          <div class="card card-xl-stretch mb-xl-8">
            <div class="card-header border-0">
              <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">
                  <span class="svg-icon svg-icon-success me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
                    </svg>
                  </span>
                  Phone
                </span>
                <span class="text-muted mt-1 fw-bold fs-7 ml-2">{IDPhoneNumber}</span>
              </h3>
            </div>
            <div class="card-body pt-0">
              {
                IDPhone && IDPhone.map((item, i) => {
                  // console.log('item', item)
                  return (
                    <>
                      {
                        item && item.status === 'positive' ? (
                          <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                            <span class="svg-icon svg-icon-success me-5">
                              <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                  <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                                  <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                                </svg>
                              </span>
                            </span>
                            <div class="flex-grow-1 me-2">
                              <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">{item.number_status}</a>
                              <span class="text-muted fw-bold d-block">{item.message}</span>
                            </div>
                            <span className="svg-icon svg-icon-1 svg-icon-success">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                                <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                              </svg>
                            </span>
                          </div>
                        ) : (
                          <>
                            {
                              item && item.status === 'warning' ? (
                                <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                                  <span class="svg-icon svg-icon-warning me-5">
                                    <span class="svg-icon svg-icon-1">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                                        <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                                      </svg>
                                    </span>
                                  </span>
                                  <div class="flex-grow-1 me-2">
                                    <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                                    <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                                  </div>
                                  <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
                                </div>
                              ) : (
                                <>
                                {
                                  item && item.status === 'negative' ? (
                                    <div class="d-flex align-items-center bg-light-danger rounded p-5 mb-7">
                                    <span class="svg-icon svg-icon-danger me-5">
                                      <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                          <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                                          <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                                        </svg>
                                      </span>
                                    </span>
                                    <div class="flex-grow-1 me-2">
                                      <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                                      <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                                    </div>
                                    <span class="fw-bolder text-danger py-1"><i class="bi bi-exclamation-triangle-fill text-danger min-w-30px fsu" /></span>
                                  </div>

                                    ):null
                                }
                                </>
                                
                              )
                            }
                          </>
                        )

                      }
                    </>
                  )
                }
                )
              }
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-3 col-sm-3">
          <div class="card card-xl-stretch mb-xl-8">
            <div class="card-header border-0">
              <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">
                  <span class=" svg-icon-email svg me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                      <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z" />
                    </svg>
                  </span>
                  Email
                </span>
                <span class="text-muted mt-1 fw-bold fs-7 ml-2">javin05@gmail.com</span>
              </h3>
            </div>
            <div class="card-body pt-0">
              <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                <span class="svg-icon svg-icon-warning me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
              <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                <span class="svg-icon svg-icon-warning me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
            </div>
          </div>

        </div>
        <div className="col-lg-3 col-md-3 col-sm-3">
          <div class="card card-xl-stretch mb-xl-8">
            <div class="card-header border-0">
              <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">
                  <span class=" svg-icon-address svg me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                      <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
                    </svg>
                  </span>
                  Address
                </span>
                <span class="text-muted mt-1 fw-bold fs-7 ml-2">No 160 adayar chennai</span>
              </h3>
            </div>
            <div class="card-body pt-0">
              <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                <span class="svg-icon svg-icon-warning me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
              <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                <span class="svg-icon svg-icon-warning me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
            </div>
          </div>

        </div>
        <div className="col-lg-3 col-md-3 col-sm-3">
          <div class="card card-xl-stretch mb-xl-8">
            <div class="card-header border-0">
              <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">
                  <span class=" svg-icon-location svg me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                      <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
                    </svg>
                  </span>
                  IP Address
                </span>
                <span class="text-muted mt-1 fw-bold fs-7 ml-2">78.654.45.975</span>
              </h3>
            </div>
            <div class="card-body pt-0">
              <div class="d-flex align-items-center bg-light-warning rounded p-5 mb-7">
                <span class="svg-icon svg-icon-warning me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Group lunch celebration</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span class="fw-bolder text-warning py-1"><i class="bi bi-exclamation-triangle-fill text-warning min-w-30px fsu" /></span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
              <div class="d-flex align-items-center bg-light-success rounded p-5 mb-7">
                <span class="svg-icon svg-icon-success me-5">
                  <span class="svg-icon svg-icon-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path opacity="0.3" d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z" fill="black"></path>
                      <path d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z" fill="black"></path>
                    </svg>
                  </span>
                </span>
                <div class="flex-grow-1 me-2">
                  <a href="#" class="fw-bolder text-gray-800 text-hover-primary fs-6">Navigation optimization</a>
                  <span class="text-muted fw-bold d-block">Due in 2 Days</span>
                </div>
                <span className="svg-icon svg-icon-1 svg-icon-success">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                    <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"></path>
                  </svg>
                </span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  const { preventionStore, editAlertsStore } = state

  return {
    preventionAlerDetails:
      preventionStore &&
        preventionStore.preventionAlerDetails
        ? preventionStore.preventionAlerDetails
        : {},
  }

}

const mapDispatchToProps = (dispatch) => ({
  getPrevAlertDetailsDispatch: (id) =>
    dispatch(preventionAlertDetailsActions.getPrevAlertDetails(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Merchant);
