import React, { useState, useEffect } from 'react'
import 'bootstrap-icons/font/bootstrap-icons.css'
import { connect } from 'react-redux'
import { KTSVG } from '../../theme/helpers'
import _ from 'lodash'
import { riskManagementActions } from '../../store/actions'

function SearchList(props) {
  const { getRiskMgmtColumns } = props
  const [, setShow] = useState(false)

  const [formData, setFormData] = useState({
    deviceID: '',
    phone: '',
    email: '',
    ipAddress: '',
    address: '',
    status: ''
  })

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value})
  }

  const handleSearch = () => {
    setShow(false)
    const params = {}
    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key] !== '') {
        params[key] = formData[key]
      }
    }
    getRiskMgmtColumns(params)
  }

  const handleReset = () => {
    setFormData({
      deviceID: '',
      phone: '',
      email: '',
      ipAddress: '',
      address: '',
      status: ''
    })
    const params = {
      limit: 25,
      page: 1
    }
    getRiskMgmtColumns(params)
  }

  return (
    <>
      <div>
        <button
          type='button'
          className='btn btn-sm btn-light-primary btn-responsive font-5vw me-3 pull-right'
          data-toggle='modal'
          data-target='#searchModal'
          onClick={() => { setShow(true) }}
        >
          {/* eslint-disable */}
          <KTSVG path='/media/icons/duotune/general/gen021.svg' />
          {/* eslint-disable */}
          Search
        </button>
      </div>
      <div
        className='modal fade'
        id='searchModal'
        tabIndex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden="''"
        data-backdrop="static"
        data-keyboard="false"
      >
        <div className='modal-dialog modal-dialog-centered mw-1000px'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h2 className='me-8'>Search</h2>
              <button
                type='button'
                className='btn btn-lg btn-icon btn-active-light-primary close'
                data-dismiss='modal'
                onClick={() => { setShow(false) }}
              >
                {/* eslint-disable */}
                <KTSVG path='/media/icons/duotune/arrows/arr061.svg' className='svg-icon-1' />
                {/* eslint-disable */}
              </button>
            </div>
            <div className='modal-body'>
              <form className='container-fixed'>
                <div className='card-header bg-secondary'>
                  <div className='card-body'>
                    <div className='form-group row mb-4'>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Device ID:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='deviceID'
                            type='text'
                            className='form-control'
                            placeholder='Device ID'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.deviceID || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Individual Phone:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='phone'
                            type='text'
                            className='form-control'
                            placeholder='Individual Phone'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.phone || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Individual Email:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='email'
                            type='text'
                            className='form-control'
                            placeholder='Individual Email'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.email || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          IP Address:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='ipAddress'
                            type='text'
                            className='form-control'
                            placeholder='IP Address'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.ipAddress || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                         Address:
                        </label>
                        <div className='col-lg-11'>
                          <input
                            name='address'
                            type='text'
                            className='form-control'
                            placeholder='Address'
                            onChange={(e) => handleChange(e)}
                            autoComplete='off'
                            value={formData.address || ''}
                          />
                        </div>
                      </div>
                      <div className='col-lg-3 mb-3'>
                        <label className='font-size-xs font-weight-bold mb-3  form-label'>
                          Status:
                        </label>
                        <div className='col-lg-11'>
                          <select
                            name='status'
                            className='form-select form-select-solid'
                            data-control='select'
                            data-placeholder='Select an option'
                            data-allow-clear='true'
                            onChange={(e) => handleChange(e)}
                            value={formData.status || ''}
                          >
                            <option value=''>Select...</option>
                            <option value='ACTIVE'>ACTIVE</option>
                            <option value='INACTIVE'>INACTIVE</option>
                          </select>
                        </div>
                      </div>
                      <div className='form-group row mb-4'>
                        <div className='col-lg-6' />
                        <div className='col-lg-6'>
                          <div className='col-lg-11'>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-primary m-2 fa-pull-right'
                              onClick={() => handleSearch()}
                              data-dismiss='modal'
                            >
                              Search
                            </button>
                            <button
                              type='button'
                              className='btn btn-sm btn-light-danger m-2 fa-pull-right close'
                              onClick={() => handleReset()}
                            >
                              Reset
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


const mapStateToProps = state => ({
  getRiskManagementlist: state && state.riskManagementlistStore && state.riskManagementlistStore.getRiskManagementlist,
  loading: state && state.riskManagementlistStore && state.riskManagementlistStore.loading,
})

const mapDispatchToProps = dispatch => ({
  getRiskMgmtColumns: (data) =>
    dispatch(riskManagementActions.getRiskManagementlist(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList)